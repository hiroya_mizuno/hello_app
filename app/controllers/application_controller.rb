class ApplicationController < ActionController::Base
  before_action :authenticate_user! ,only: [:index, :edit, :update, :show, :destroy, :following, :followers]
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  
  protected

    def configure_permitted_parameters
      added_attrs = [ :fullname, :username, :email, :password, :password_confirmation ]
      devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
      devise_parameter_sanitizer.permit :account_update, keys: [:fullname, :username, :email, :password, :password_confirmation, :url, :phonenumber, :introduction, :sex]
      devise_parameter_sanitizer.permit :sign_in, keys: added_attrs
    end

end
