require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(fullname: "Example User", username: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
  end
  
  
  # models/user.rbにdependent: :destroyを追加しユーザーが削除されたときに、そのユーザーに紐付いた (そのユーザーが投稿した) マイクロポストも一緒に削除されるようになる。
  # これは、管理者がシステムからユーザーを削除したとき、持ち主の存在しないマイクロポストがデータベースに取り残されてしまう問題を防ぐ
  #test "associated microposts should be destroyed" do 
    #@user.save
    #@user.microposts.create!(content: "Lorem ipsum")
    #assert_difference 'Micropost.count', -1 do
      #@user.destroy
    #end
  #end
  #以上まで
  
  test "should follow and unfollow a user" do
    michael = users(:michael)
    archer  = users(:archer)
    assert_not michael.following?(archer)
    michael.follow(archer)
    assert michael.following?(archer)
    assert archer.followers.include?(michael)
    michael.unfollow(archer)
    assert_not michael.following?(archer)
  end
  
  test "feed should have the right posts" do
    michael = users(:michael)
    archer  = users(:archer)
    lana    = users(:lana)
    # フォローしているユーザーの投稿を確認
    lana.microposts.each do |post_following|
      assert michael.feed.include?(post_following)
    end
    # 自分自身の投稿を確認
    michael.microposts.each do |post_self|
      assert michael.feed.include?(post_self)
    end
    # フォローしていないユーザーの投稿を確認
    archer.microposts.each do |post_unfollowed|
      assert_not michael.feed.include?(post_unfollowed)
    end
  end
  
end
