１苦労した点
・deviseを導入してrails tuturialとの差を確認しながら開発を進めていかなくてはならないため、deviseの挙動を正確に把握する必要がある点
・様々なウェブページやtuturialを参考にしながらも、ロジックを正確に理解するのにとにかく時間がかってしまった
・一つ一つ理解していないとエラーが多発してしまう。

２学んだ点
・エラー対応：とにかくログを頼りに原因を探ることが大事だと痛感した
・コンフリクトの修正
・テストを書かずに続けていると予期せぬ実装漏れが出そう

３自慢できる点
自慢できる点は特にないですが、自分でできる範囲で理解しながらじっくり実装に取り組むように心がけてきた。
ただ、エラーが出た際単純亜ミスであるにもかかわらず見つけられずに時間をかけすぎてしまった点もあり未実装の点がまだまだ多く残りまだまだ吸収しなければならないことがたくさんあると思う。