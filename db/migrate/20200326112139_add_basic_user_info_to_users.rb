class AddBasicUserInfoToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :fullname, :string, null: false, default: ""
    add_column :users, :username, :string, null: false, default: ""
    add_column :users, :url, :text
    add_column :users, :introduction, :text
    add_column :users, :phonenumber, :string
    add_column :users, :sex, :integer
  end
end
