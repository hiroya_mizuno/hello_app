Rails.application.routes.draw do

  #get 'microposts/index'

  #get 'microposts/show'

  #get 'users/index'

  #get 'users/show'
  
  #get 'users/edit'


  root 'static_pages#home'
  
  devise_for :users, controllers: {
    confirmations: 'users/confirmations',
    registrations: 'users/registrations',
    sessions: 'users/sessions'
  }
  
  resources :users, only: [:index, :show, :edit, :update] do
    member do
      get :following, :followers
    end
  end
  resources :microposts, only: [:index, :show, :create, :destroy]
  resources :relationships,       only: [:create, :destroy]

end
